# DevPro 5 Partes de um Link



## 1 - Domínio

Endereço utilizado na internado para acessar a página do site.
Ex: [http://www.pudim.com.br/](http://www.pudim.com.br/)


## 2 - Protocolo de Comunicação

http: Protocolo inseguro, dados pode ser acessados, caso a requisição seja interseptada.
https: Protocolo Seguro, requisição vai ser encriptada.

* Exemplos:
```
URL: http://www.pudim.com.br/
Protocolo: http
Domínio: www.pudim.com.br

URL: https://ge.globo.com/
Protocolo: https
Domínio: ge.globo.com
```


Tudo que vem depois do http, é considerado domínio.



## 3 - Caminho - PATH

É sufixo, serve para indicar o recurso que está sendo acessado dentro do servidor.
Começa na barra, logo depois do domínio;

Exemplos:
```
URL: http://www.pudim.com.br/
Protocolo: http
Domínio: www.pudim.com.br
Path: /

URL: https://ge.globo.com/
Protocolo: https
Domínio: ge.globo.com
Path: /

URL: https://ge.globo.com/sp/santos-e-regiao/futebol/times/santos/
Protocolo: https
Domínio: ge.globo.com
Path: /sp/santos-e-regiao/futebol/times/santos/

```



## 4 - Query String

Uma outra ação comum que fazemos no dia a dia é executar uma busca no Google.
Ao abrir uma página, retornada da busca pecebemos:
Depois do path, existe uma letra “?”
Tudo que vem depois do ponto de interogação, são parametros utilizados para identificar qual pesquisa
está sendo feita, divididas pelo &. 

```
URL: http://www.pudim.com.br/
Protocolo: http
Domínio: www.pudim.com.br
Path: /
Parâmetros: Não tem

URL: https://ge.globo.com/
Protocolo: https
Domínio: ge.globo.com
Path: /
Parâmetros: Não tem

URL: https://ge.globo.com/sp/santos-e-regiao/futebol/times/santos/
Protocolo: https
Domínio: ge.globo.com
Path: /sp/santos-e-regiao/futebol/times/santos/
Parâmetros: Não tem

URL: https://www.google.com/search?q=Comunidade+DevPro&client=firefox-b-d
Protocolo: https
Domínio: www.google.com
Path: /search
Parâmetros:
1. Nome: q Valor: Comunidade+DevPro
2. Nome: client - Valor: firefox-b-d
```
## 5 - Porta

Vem do ingles port, mas o seu significado é porto, falso cognato.
Cada protocolo tem uma porta padrão, caso não informada, é utilizada a padrão.


## Onde Cadastrar os domínios

Todo disposito conectado a rede precisa de um endereço IP.

Para saber seu [IP](http://www.whatismyip.com/)

Para ajudar a memorizar os endereços, foi criado o DNS (Domain Name Server)
que conecta o domínio ao respectivo IP.

Para organizar a intert foram criadas organizações reponsáveis pela gestão e comercialização
desses nomes domínios, no Brasil o responsável por controlar o .br é o NIC (Nucleo de Informação e 
Coordenação).
NIC oferece oserviço [registro.br](https://registro.br) para comprar domínios.
Ele também pode delegar outras organizações para fornecer o mesmo serviço.

Comprando o domínio você pode utilizar como serviço de DNS o próprio registro.br.
Mas se precisar outras funcionalidades mais avançadas, pode utilizar [cloudflare](https://www.cloudflare.com/), basta fazer o aptamento no registro.br do cloudflare como serviço de DNS do seu domínio.

CNAME (Canonical Name) Redireciona o IP para outro domínio.

## Curiosidade

www (world wide web) subdomínio mais utilizado na internet, recursos disponibilizados para o mundo todo,
ex: www.google.com




